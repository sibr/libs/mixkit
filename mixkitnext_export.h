
#ifndef MIXKITNEXT_EXPORT_H
#define MIXKITNEXT_EXPORT_H

#ifdef MIXKITNEXT_STATIC_DEFINE
#  define MIXKITNEXT_EXPORT
#  define MIXKITNEXT_NO_EXPORT
#else
#  ifndef MIXKITNEXT_EXPORT
#    ifdef MixKitNext_EXPORTS
        /* We are building this library */
#      define MIXKITNEXT_EXPORT __declspec(dllexport)
#    else
        /* We are using this library */
#      define MIXKITNEXT_EXPORT __declspec(dllimport)
#    endif
#  endif

#  ifndef MIXKITNEXT_NO_EXPORT
#    define MIXKITNEXT_NO_EXPORT 
#  endif
#endif

#ifndef MIXKITNEXT_DEPRECATED
#  define MIXKITNEXT_DEPRECATED __declspec(deprecated)
#endif

#ifndef MIXKITNEXT_DEPRECATED_EXPORT
#  define MIXKITNEXT_DEPRECATED_EXPORT MIXKITNEXT_EXPORT MIXKITNEXT_DEPRECATED
#endif

#ifndef MIXKITNEXT_DEPRECATED_NO_EXPORT
#  define MIXKITNEXT_DEPRECATED_NO_EXPORT MIXKITNEXT_NO_EXPORT MIXKITNEXT_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef MIXKITNEXT_NO_DEPRECATED
#    define MIXKITNEXT_NO_DEPRECATED
#  endif
#endif

#endif /* MIXKITNEXT_EXPORT_H */
